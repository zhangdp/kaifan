
#非常感谢你的贡献

## 一个为了解决鹰漠集体吃饭的微信公众号HTML5项目。。。。。

## 依赖环境

你必须安装以下工具.

  * [Node.js](http://nodejs.org): 全栈攻城师的最爱.
  * [Grunt](http://gruntjs.com/): 前端小王子必备 `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): 前端小王子必备 `[sudo] npm install -g bower`
  
  你有可能需要以下两个镜像:
  * [NPM 淘宝镜像](http://npm.taobao.org/)
  * [ruby GEM 淘宝镜像](http://ruby.taobao.org/)
  
  这是一个Foundation 项目

## 快速开始

```bash
git clone https://zhangdp@bitbucket.org/zhangdp/kaifan.git
npm install && bower install
cd kaifan
grunt build
grunt
```

## 文件说明

  * `scss/_settings.scss`: Foundation 设置
  * `scss/app.scss`: 应用设置
  * `foundation-help.html`: Foundation 介绍
  
## 需求说明

订餐需求描述

1，菜单
      需要有以下字段：菜单编号、菜单标题、菜单内容、供应商、更新时间。
      支持查询、录入、编辑、删除。并需要支持选中菜单发起生成点餐订单。

2，订单
      需要有以下字段：订单编号、所选菜单编号，所选菜单标题，下单用户，对应供应商，订餐时间，订单状态。
      订单状态有 待处理 已取消 已生效。待处理订单支持取消，然后用户可以重新下单。
      已生效订单需要支持导出成表格，筛选时间范围3小时内，按供应商分别排序。（用来分别发给不同的餐馆）

3，菜单展示列表
      由后台控制开启截止订餐按钮，每次行政更新确认完菜单后开启订餐，到时间后关闭订餐操作。
     开启期间用户可以下单，关闭后所有待处理订单变成已生效状态，用户不能再操作下单(也不能取消已生效订单)。

4，其它常规模块，用户注册登录和历史订单查询，后台统计。

5，展望
      以后可能需要开发菜单更新接口 和 订单查询接口，与外卖商对接。



#再次感谢你的贡献
